-- Creazione del Database
CREATE DATABASE DistilleryDB OWNER Distillery ENCODING = 'UTF8';

-- Creo un dominio per l'indirizzo email
CREATE DOMAIN emailaddress AS character varying(254)
CONSTRAINT properemail CHECK (((VALUE)::text ~* '^[A-Za-z0-9._%-]+@[A-Za-z0-9.- ]+[.][A-Za-z]+$'::text));


-- Creo le tabelle

CREATE TABLE Person(
FiscalCode VARCHAR(20),	
Surname VARCHAR(20) NOT NULL,
Name VARCHAR(20) NOT NULL,
Gender CHAR(1) NOT NULL,
DateOfBirth DATE NOT NULL,
ResidenceAddress VARCHAR(50) NOT NULL,
BirthAddress VARCHAR(50) NOT NULL,
email VARCHAR(50),
PhoneNumber VARCHAR(20),
BadgeNumber VARCHAR(20),
FBAccount VARCHAR(50),
PRIMARY KEY (FiscalCode)
);

CREATE TABLE TouristicActivity(
TouristicActivityID SERIAL,	
Name VARCHAR(40) NOT NULL,
ExtimatedLength FLOAT NOT NULL,
PRIMARY KEY (TouristicActivityID)
);

CREATE TABLE Groups(
GroupsID SERIAL,	
Color VARCHAR(20) NOT NULL,
PRIMARY KEY (GroupsID)
);

CREATE TABLE Media(
MediaID SERIAL,	
Name VARCHAR(20) NOT NULL,
PRIMARY KEY (MediaID)
);

CREATE TABLE Campaign(
CampaignID SERIAL,	
Name VARCHAR(40) NOT NULL,
Date DATE NOT NULL,
PRIMARY KEY (CampaignID)
);

CREATE TABLE Topic(
TopicID SERIAL,	
Name VARCHAR(20) NOT NULL,
PRIMARY KEY (TopicID)
);

CREATE TABLE Tour(
TourID SERIAL,
StartDayTime TIMESTAMP NOT NULL,
GroupsID INTEGER NOT NULL,	
FiscalCode VARCHAR(20) NOT NULL,
PRIMARY KEY (TourID),
FOREIGN KEY (FiscalCode) REFERENCES Person(FiscalCode),
FOREIGN KEY (GroupsID) REFERENCES Groups(GroupsID)
);

CREATE TABLE Compose(
OrderNumber INTEGER NOT NULL,
StartDayTime TIMESTAMP NOT NULL,
EndDayTime TIMESTAMP NOT NULL,	
TourID INTEGER,
TouristicActivityID INTEGER,
PRIMARY KEY (TourID,TouristicActivityID),
FOREIGN KEY (TourID) REFERENCES Tour(TourId),
FOREIGN KEY (TouristicActivityID) REFERENCES							TouristicActivity(TouristicActivityID)
);

CREATE TABLE IsOn(
CampaignID INTEGER,
MediaID INTEGER,
Content TEXT NOT NULL,
PRIMARY KEY (CampaignID, MediaID),
FOREIGN KEY (CampaignID) REFERENCES Campaign(CampaignID),
FOREIGN KEY (MediaID) REFERENCES Media(MediaID)
);

CREATE TABLE IsAbout(
TopicID INTEGER,
CampaignID INTEGER,
PRIMARY KEY (CampaignID, TopicID),
FOREIGN KEY (CampaignID) REFERENCES Campaign(CampaignID),
FOREIGN KEY (TopicID) REFERENCES Topic(TopicID)
);

CREATE TABLE IsDirectedTo(
CampaignID INTEGER,
FiscalCode VARCHAR(20),
PRIMARY KEY (CampaignID, FiscalCode),
FOREIGN KEY (CampaignID) REFERENCES Campaign(CampaignID),
FOREIGN KEY (FiscalCode) REFERENCES Person(FiscalCode)
);

CREATE TABLE Forms(
GroupsID INTEGER,
FiscalCode VARCHAR(20),
PRIMARY KEY (GroupsID, FiscalCode),
FOREIGN KEY (GroupsID) REFERENCES Groups(GroupsID),
FOREIGN KEY (FiscalCode) REFERENCES Person(FiscalCode)
);

CREATE TABLE WorkStation (
WorkStationID SERIAL,
PRIMARY KEY(WorkStationID)
);

CREATE TABLE WorkingActivity (
WorkingActivityID SERIAL,
Name varchar(20) NOT NULL,
Description varchar(20) NOT NULL,
PRIMARY KEY(WorkingActivityID)
);

CREATE TABLE RawMaterial (
RawMaterialID SERIAL,
Name varchar(20) NOT NULL,
Quantity INTEGER NOT NULL,
PRIMARY KEY(RawMaterialID)
);

CREATE TABLE Barrel (
BarrelID SERIAL,
WoodType varchar(20) NOT NULL,
Capacity INTEGER NOT NULL,
PRIMARY KEY(BarrelID)
);

CREATE TABLE Instrument(
InstrumentID SERIAL,
Name VARCHAR(20) NOT NULL,
WorkStationID INTEGER NOT NULL,
PRIMARY KEY(InstrumentID),
FOREIGN KEY(WorkStationID) REFERENCES WorkStation(WorkStationID)
);

CREATE TABLE Damage(
DamageID SERIAL,
Note varchar(100),
InstrumentID INTEGER NOT NULL,
PRIMARY KEY(DamageID),
FOREIGN KEY(InstrumentID) REFERENCES Instrument(InstrumentID)
);

CREATE TABLE Batch(
BatchID SERIAL,
WorkstationID INTEGER NOT NULL,
PRIMARY KEY(BatchID),
FOREIGN KEY(WorkStationID) REFERENCES WorkStation(WorkStationID)
);

CREATE TABLE Participate(
BatchID INTEGER,
WorkingActivityID INTEGER,
FiscalCode varchar(20),
StartingDate TIMESTAMP NOT NULL,
EndingDate TIMESTAMP NOT NULL,
PRIMARY KEY(BatchID, WorkingActivityID, FiscalCode),
FOREIGN KEY(BatchID) REFERENCES Batch(BatchID),
FOREIGN KEY(WorkingActivityID) REFERENCES WorkingActivity(WorkingActivityID),
FOREIGN KEY(FiscalCode) REFERENCES Person(FiscalCode)
);

CREATE TABLE States(
BatchID INTEGER,
WorkingActivityID INTEGER,
State INTEGER,
PRIMARY KEY(BatchID, WorkingActivity),
FOREIGN KEY(BatchID) REFERENCES Batch(BatchID),
FOREIGN KEY(WorkingActivityID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Recipe(
RecipeID SERIAL,
ProductName VARCHAR(20),
Alcohol INTEGER,
PRIMARY KEY(RecipeID)
);

CREATE TABLE Determine(
WorkingActivityID INTEGER,
RecipeID INTEGER,
Phase INTEGER,
PRIMARY KEY(WorkingActivityID, RecipeID),
FOREIGN KEY(WorkingActivityID) REFERENCES WorkingActivity(WorkingActivityID),
FOREIGN KEY(RecipeID) REFERENCES Recipe(RecipeID)
);

CREATE TABLE Fermentation(
FermentationID INTEGER,
Duration INTEGER,
PRIMARY KEY(FermentationID),
FOREIGN KEY(FermentationID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Ferment(
FermentationID INTEGER,
RawMaterialID INTEGER,
Quantity integer,
PRIMARY KEY(FermentationID, RawMaterialID),
FOREIGN KEY(FermentationID) REFERENCES Fermentation(FermentationID),
FOREIGN KEY(RawMaterialID) REFERENCES RawMaterial(RawMaterialID)
);

CREATE TABLE Distillation(
DistillationID INTEGER,
Phase INTEGER,
BoilingTemperature REAL,
PRIMARY KEY(DistillationID),
FOREIGN KEY(DistillationID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Dilution(
DilutionID INTEGER,
StartingAlcohol INTEGER,
EndingAlcohol INTEGER,
PRIMARY KEY(DilutionID),
FOREIGN KEY(DilutionID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Aging(
AgingID INTEGER,
Months INTEGER NOT NULL,
PRIMARY KEY(AgingID),
FOREIGN KEY(AgingID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Realize(
BarrelID INTEGER,
AgingID INTEGER,
PRIMARY KEY(BarrelID, AgingID),
FOREIGN KEY(BarrelID) REFERENCES Barrel(BarrelID),
FOREIGN KEY(AgingID) REFERENCES Aging(AgingID)
);

CREATE TABLE Bottling(
BottlingID INTEGER,
Liters INTEGER NOT NULL,
PRIMARY KEY(BottlingID),
FOREIGN KEY(BottlingID) REFERENCES WorkingActivity(WorkingActivityID)
);

CREATE TABLE Bottle(
	BottleID SERIAL,
	Name VARCHAR(30) NOT NULL,
Serial SERIAL NOT NULL,
Price NUMERIC(6,2) NOT NULL,
Capacity INTEGER NOT NULL,
BottlingID INTEGER NOT NULL,
PRIMARY KEY(BottleID),
FOREIGN KEY(BottlingID) REFERENCES Bottling(BottlingID)
);

CREATE TABLE AgedOn(
AgedOnID INTEGER,
BarrelID INTEGER NOT NULL,
PRIMARY KEY(AgedOnID),
FOREIGN KEY(BarrelID) REFERENCES Barrel
);

CREATE TABLE Salary(
	TransactionID INTEGER,
	FiscalCode VARCHAR(20),
PRIMARY KEY(TransactionID),
FOREIGN KEY(FiscalCode) REFERENCES Person,
FOREIGN KEY(TransactionID) REFERENCES Transaction
);

CREATE TABLE Transaction(
	TransactionID SERIAL,
	TypePayment VARCHAR(20) NOT NULL,
	Date DATE NOT NULL,
	TotalAmount NUMERIC(8 ,2) NOT NULL,
	PRIMARY KEY(TransactionID)
);

CREATE TABLE Company(
Name VARCHAR(40) NOT NULL,
City VARCHAR(20) NOT NULL,
IBAN VARCHAR(34) NOT NULL,
Address VARCHAR(50) NOT NULL,
VAT_ID VARCHAR(20),
Role VARCHAR(8) NOT NULL,
FiscalCode VARCHAR(20),
PRIMARY KEY(Name, VAT_ID),
FOREIGN KEY(FiscalCode) REFERENCES Person
);

CREATE TABLE Sale(
	BottleID INTEGER,
	TransactionID INTEGER NOT NULL,
	PRIMARY KEY(BottleID),
	FOREIGN KEY(TransactionID) REFERENCES Transaction,
	FOREIGN KEY(BottleID) REFERENCES Bottle
);

CREATE TABLE CompanyTransaction(
	Name VARCHAR(40) NOT NULL,
	TransactionID INTEGER,
	VAT_ID VARCHAR(20),
	PRIMARY KEY(TransactionID),
	FOREIGN KEY(Name, VAT_ID) REFERENCES Company,
	FOREIGN KEY(TransactionID) REFERENCES Transaction
);

CREATE TABLE Purchase(
	RawMaterialID INTEGER NOT NULL,
	TransactionID INTEGER NOT NULL,
	Quantity REAL NOT NULL,
	PRIMARY KEY(RawMaterialID, TransactionID),
	FOREIGN KEY(RawMaterialID) REFERENCES RawMaterial,
	FOREIGN KEY(TransactionID) REFERENCES Transaction
);
