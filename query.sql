-- Starting from a bottle (with id 'bid') retrieve the id of the working activities involved in its production process.

SELECT DISTINCT WorkingActivityID FROM Participate WHERE BatchID = (SELECT DISTINCT BatchID FROM Participate WHERE WorkingActivityID = (SELECT BottlingID FROM Bottle WHERE BottleID= bid));



-- Starting from a batch (having its id ‘bid’) determine if it is still in progress or is suspended (state 1->in progress, state 3->suspended).

SELECT DISTINCT Name, Description, State FROM Participate INNER JOIN States ON Participate.BatchID=States.BatchID INNER JOIN WorkingActivity ON Participate.WorkingActivityID=WOrkingActivity.WorkingActivityID WHERE (Participate.BatchID=bid and (State = 1 OR State = 3));

-- Get the list of the free workstations
-- This query is required every time there is the necessity to start a new working process in order to figure out where this process could be computed. From the list of all the workstations we exclude the ones which belong to batches still in progress.

SELECT WorkStationID FROM Workstation EXCEPT (SELECT WorkstationID FROM Batch INNER JOIN States ON Batch.BatchID=States.BatchID WHERE States.state=1 EXCEPT (SELECT WorkstationID FROM Batch INNER JOIN States ON Batch.BatchID=States.BatchID INNER JOIN Aging ON Aging.agingid=States.workingactivityid WHERE States.state=1)) ORDER BY WorkStationID;


-- Get a list of all activities of a day
-- This can be achieved by first selecting all the activities in the same day by looking Compose table. Supposing that we choose a day ‘d’ (in a format ‘yyyy-mm-dd’) we have to create two variables beginDay = ‘d’+‘ 00:00:00’ and endDay = ‘d’+’ 23:59:59’. In both cases ‘+’ operation will concatenate the two strings for creating two timestamps, one representing the start of the day ‘d’ and the other the end of the same day. The query follows

SELECT DISTINCT TourID FROM Compose WHERE (StartDayTime > beginDay AND EndDayTime < endDay);

-- The query above gets only IDs while, if requested, it is possible to get all information about that tour by using

SELECT DISTINCT Tour.TourID, Person.Name, Person.Surname, Groups.Color FROM ((Compose INNER JOIN Tour ON Compose.TourID=Tour.TourID) INNER JOIN Person ON Tour.FiscalCode=Person.FiscalCode) INNER JOIN groups ON Tour.GroupsID=groups.GroupsID WHERE (Compose.StartDayTime > beginDay AND Compose.EndDayTime < endDay);

-- Get the profit of the day

SELECT sum (totalamount) , max (date) AS today
 FROM transaction
 WHERE date = (SELECT max (date) FROM transaction);

-- Get the warehouse stock
-- This query constantly check the warehouse stock, indentifying the type of raw materials that are ending.

SELECT name, quantity
 FROM rawmaterial
  WHERE quantity < 50;

--  This query returns the name of the bottles sold to a company (name = ‘Inc’‘Sidi’).

SELECT Name FROM Bottle WHERE (BottleID = )SELECT DISTINCT Bottle.name from Bottle inner join Sale on Bottle.bottleid=Sale.bottleid inner join CompanyTransaction on CompanyTransaction.transactionid=Sale.transactionid where CompanyTransaction.name = 'Sidi';
