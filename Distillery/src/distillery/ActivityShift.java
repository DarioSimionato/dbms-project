/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distillery;

import java.sql.Date;
import java.sql.Time;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author deppo
 */
public class ActivityShift {
    protected int workingActivityID;
    protected int batchID;
    protected int state;
    public static String[] status = new String[]{"PLANNED", "ON RUNNING", "COMPLETED", "SUSPENDED"};
    protected Time startingDate;
    protected Time endingDate;
    protected String name;
    protected String description;
    protected int workStationID;

    public ActivityShift(int workingActivityID, int batchID, int state, Time startingDate, Time endingDate, String name, String description, int workStationID) {
        this.workingActivityID = workingActivityID;
        this.batchID = batchID;
        this.state = state;
        this.startingDate = startingDate;
        this.endingDate = endingDate;
        this.name = name;
        this.description = description;
        this.workStationID = workStationID;
    }

    @Override
    public String toString() {
        return name+", "+description+", at workstation "+workStationID+" from "+startingDate.getHours()+" to "+endingDate.getHours()+" - STATE: "+status[state];
    }

    public int getState() {
        return state;
    }

    public int getBatchID() {
        return batchID;
    }

    public int getWorkingActivityID() {
        return workingActivityID;
    }
    
    
    
    
    
    
    

    

    
    
    
}
