/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distillery;

/**
 *
 * @author Dario
 */
public class BottleType {
    private String nome;
    private int quantity;
    private double price;
    private int capacity;

    public BottleType( String nome, double price, int capacity, int quantity) {
        this.nome = nome;
        this.price = price;
        this.capacity = capacity;
        this.quantity = quantity;
    }


    public String getNome() {
        return nome;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getPrice() {
        return price;
    }

    public int getCapacity() {
        return capacity;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
    
    @Override
    public String toString() {
        return nome + ", "+ capacity + " ml, "+price+"€, "+quantity+" bott";
    }
    
}
