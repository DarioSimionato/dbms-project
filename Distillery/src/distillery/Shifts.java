/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distillery;

import java.awt.event.ActionEvent;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author deppo
 */
public class Shifts extends javax.swing.JFrame {

    protected final String[] months = new String[]{"Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio",
        "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"};
    protected final int[] years = new int[]{2018, 2019};
    protected String fiscalCode;

    /**
     * Creates new form Shifts
     */
    public Shifts(String fc) {
        initComponents();
        fiscalCode = fc;
        jComboBox1.removeAllItems();
        jComboBox2.removeAllItems();
        jComboBox3.removeAllItems();
        GregorianCalendar gc = new GregorianCalendar();
        for (int i = 0; i < months.length; i++) {
            jComboBox2.addItem(months[i]);
        }
        for (int i = 0; i < years.length; i++) {
            jComboBox3.addItem(years[i]);
        }
        for (int i = 1; i <= gc.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            jComboBox1.addItem(i);
        }
        jComboBox2.setSelectedIndex(gc.get(Calendar.MONTH));
        jComboBox1.setSelectedIndex(gc.get(Calendar.DAY_OF_MONTH) - 1);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jComboBox3 = new javax.swing.JComboBox();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jLabel1.setText("Day:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel2.setText("Month:");

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox2.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                jComboBox2PopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });

        jLabel3.setText("Year:");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jButton1.setText("shifts");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jList1.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jList1ValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList1);

        jButton3.setText("Start Activity");
        jButton3.setEnabled(false);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Complete Activity");
        jButton4.setEnabled(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Suspend Activity");
        jButton5.setEnabled(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton5)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton3)
                    .addComponent(jButton4)
                    .addComponent(jButton5))
                .addContainerGap(77, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox2PopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_jComboBox2PopupMenuWillBecomeInvisible
        int sel = jComboBox1.getSelectedIndex();
        GregorianCalendar gc = new GregorianCalendar(2018, jComboBox2.getSelectedIndex(), 1);
        jComboBox1.removeAllItems();
        for (int i = 1; i <= gc.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            jComboBox1.addItem(i);
        }
        try {
            jComboBox1.setSelectedIndex(sel);
        } catch (Exception ex) {
            jComboBox1.setSelectedIndex(0);
        }
    }//GEN-LAST:event_jComboBox2PopupMenuWillBecomeInvisible

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Connection con = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        ResultSet rs3 = null;
        ResultSet rs4 = null;

        jList1.setListData(new String[]{});
        try {
            Class.forName(Distillery.DRIVER);
            con = DriverManager.getConnection(Distillery.DATABASE, Distillery.USER, Distillery.PASSWORD);

            String y = "" + jComboBox3.getSelectedItem();
            int m = (jComboBox2.getSelectedIndex() + 1);
            String mm = "" + m;
            if (m < 10) {
                mm = "0" + mm;
            }
            int d = jComboBox1.getSelectedIndex() + 1;
            String dd = "" + d;
            if (d < 10) {
                dd = "0" + dd;
            }
            String start = y + "-" + mm + "-" + dd + " 00:00:00";
            String end = y + "-" + mm + "-" + dd + " 23:59:59";
            String sql = "SELECT batchid, workingactivityid, startingdate, endingdate FROM participate WHERE fiscalcode LIKE ? AND startingdate > '" + start + "' AND endingdate < '" + end + "' "
                    + "ORDER BY startingdate";
            pst = con.prepareStatement(sql);
            pst.setString(1, fiscalCode);
            rs = pst.executeQuery();

            ArrayList<ActivityShift> sh = new ArrayList<>();
            while (rs.next()) {
                Statement st = con.createStatement();
                rs2 = st.executeQuery("SELECT name, description FROM workingactivity WHERE workingactivityid = " + rs.getInt("workingactivityid"));
                rs2.next();

                st = con.createStatement();
                rs3 = st.executeQuery("SELECT workstationid FROM batch WHERE batchid = " + rs.getInt("batchid"));
                rs3.next();

                st = con.createStatement();
                rs4 = st.executeQuery("SELECT state FROM states WHERE batchid = " + rs.getInt("batchid") + " AND workingactivityid = " + rs.getInt("workingactivityid"));
                rs4.next();

                sh.add(new ActivityShift(rs.getInt("workingactivityid"), rs.getInt("batchid"), rs4.getInt("state"),
                        rs.getTime("startingdate"), rs.getTime("endingdate"), rs2.getString("name"), rs2.getString("description"), rs3.getInt("workstationid")));
                try {
                    st.close();
                    rs2.close();
                    rs3.close();
                    rs4.close();
                } catch (SQLException e) {
                    System.out.println("Unable to release the resources");
                } finally {
                    st = null;
                    rs2 = null;
                    rs3 = null;
                    rs4 = null;
                }
            }

            jList1.setListData(sh.toArray());

            try {
                rs.close();
            } catch (SQLException e) {
                System.out.println("Unable to release the resources");
            } finally {
                rs = null;
            }

        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                con.close();
            } catch (SQLException ex) {
                System.err.println("Unable to release resources");
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jList1ValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jList1ValueChanged
        if(!jList1.isSelectionEmpty()){
        ActivityShift as = (ActivityShift) jList1.getSelectedValue();
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        if (as.getState() == 0) {
            jButton3.setEnabled(true);
        } else if (as.getState() == 1) {
            jButton4.setEnabled(true);
            jButton5.setEnabled(true);
        }
        }
    }//GEN-LAST:event_jList1ValueChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        jButton3.setSelected(false);
        jButton4.setSelected(true);
        jButton5.setSelected(true);
        ActivityShift as = (ActivityShift) jList1.getSelectedValue();
        Connection con = null;
        Statement st = null;
        try {
            Class.forName(Distillery.DRIVER);
            con = DriverManager.getConnection(Distillery.DATABASE, Distillery.USER, Distillery.PASSWORD);
            st = con.createStatement();
            String query = "UPDATE states SET state = 1 WHERE batchid = " + as.getBatchID() + " AND workingactivityid = " + as.getWorkingActivityID();
            st.execute(query);
            int s = jList1.getSelectedIndex();
            jButton1ActionPerformed(null);
            jList1.setSelectedIndex(s);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                con.close();
                st.close();
            } catch (SQLException e) {
                System.out.println("Unable to release the resources");
            } finally {
                con = null;
                st = null;
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        jButton3.setSelected(false);
        jButton4.setSelected(false);
        jButton5.setSelected(false);
        ActivityShift as = (ActivityShift) jList1.getSelectedValue();
        Connection con = null;
        Statement st = null;
        try {
            Class.forName(Distillery.DRIVER);
            con = DriverManager.getConnection(Distillery.DATABASE, Distillery.USER, Distillery.PASSWORD);
            st = con.createStatement();
            String query = "UPDATE states SET state = 2 WHERE batchid = " + as.getBatchID() + " AND workingactivityid = " + as.getWorkingActivityID();
            st.execute(query);
            int s = jList1.getSelectedIndex();
            jButton1ActionPerformed(null);
            jList1.setSelectedIndex(s);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                con.close();
                st.close();
            } catch (SQLException e) {
                System.out.println("Unable to release the resources");
            } finally {
                con = null;
                st = null;
            }
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        jButton3.setSelected(false);
        jButton4.setSelected(false);
        jButton5.setSelected(false);
        ActivityShift as = (ActivityShift) jList1.getSelectedValue();
        Connection con = null;
        Statement st = null;
        try {
            Class.forName(Distillery.DRIVER);
            con = DriverManager.getConnection(Distillery.DATABASE, Distillery.USER, Distillery.PASSWORD);
            st = con.createStatement();
            String query = "UPDATE states SET state = 3 WHERE batchid = " + as.getBatchID() + " AND workingactivityid = " + as.getWorkingActivityID();
            st.execute(query);
            int s = jList1.getSelectedIndex();
            jButton1ActionPerformed(null);
            jList1.setSelectedIndex(s);

        } catch (Exception ex) {
            System.out.println(ex.toString());
        } finally {
            try {
                con.close();
                st.close();
            } catch (SQLException e) {
                System.out.println("Unable to release the resources");
            } finally {
                con = null;
                st = null;
            }
        }
    }//GEN-LAST:event_jButton5ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JComboBox jComboBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
