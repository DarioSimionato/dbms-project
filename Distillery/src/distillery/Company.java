/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distillery;

/**
 *
 * @author deppo
 */
public class Company {
    protected String name;
    protected String city;
    protected String address;
    protected String vat_id;

    public Company(String name, String city, String address, String vat_id) {
        this.name = name;
        this.city = city;
        this.address = address;
        this.vat_id=vat_id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }
    
    public String toString(){
        return name+", "+city+", "+address;
    }

    public String getVat_id() {
        return vat_id;
    }
    
}
