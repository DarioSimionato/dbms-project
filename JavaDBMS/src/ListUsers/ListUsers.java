package ListUsers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Date;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * List all persons who partecipated to the touristic activity 'Aperitivo' in
 * data 17-05-2018
 */
public class ListUsers {
	/*
 	The JDBC driver to be used
 	*/

	private static final String DRIVER = "org.postgresql.Driver";

	/*
 	The URL of the database to be accessed
 	*/
	private static final String DATABASE = "jdbc:postgresql://localhost/distillerydb";

	/**
 	* The username for accessing the database
 	*/
	private static final String USER = "postgres";
	/**
 	* The password for accessing the database
 	*/
	private static final String PASSWORD = "davide";
	/**
 	* The SQL statement to be executed
 	*/
	private static final String SQL = "select Person.name, Person.surname, person.dateofbirth from Person inner join Forms on Person.fiscalcode=Forms.fiscalcode \n"
        	+ "inner join Tour on Tour.GroupsID=Forms.GroupsID \n"
        	+ "inner join Compose on Compose.tourid=Tour.tourid \n"
        	+ "inner join TouristicActivity on TouristicActivity.touristicactivityid=Compose.touristicactivityid \n"
        	+ "where Compose.startdaytime > '2018-05-17 00:00:00' and Compose.enddaytime < '2018-05-17 23:59:59' and TouristicActivity.Name like 'Aperitivo'";

	public static void main(String[] args) {
    	// the connection to the DBMS
    	Connection con = null;
    	// the statement to be executed
    	Statement stmt = null;
    	// the results of the statement execution
    	ResultSet rs = null;

    	// "data structures" for the data to be read from the database
    	String name = null;
    	String surname = null;
    	Date date = null;

    	try {
        	Class.forName(DRIVER);
        	con = DriverManager.getConnection(DATABASE, USER, PASSWORD);
        	stmt = con.createStatement();
        	rs = stmt.executeQuery(SQL);
        	while (rs.next()) {
            	name = rs.getString("name");
            	surname = rs.getString("surname");
            	date = rs.getDate("dateofbirth");
            	System.out.println(name + " " + surname + " " + date);
        	}
    	} catch (SQLException e) {
        	System.out.printf("Database access error:%n");

        	while (e != null) {
            	System.err.printf("- Message: %s%n",
                    	e.getMessage());
            	System.err.printf("- SQL status code: %s%n",
                    	e.getSQLState());
            	System.err.printf("- SQL error code: %s%n",
                    	e.getErrorCode());
            	System.err.printf("%n");
            	e = e.getNextException();
        	}
    	} catch (ClassNotFoundException ex) {
        	System.err.println("DRIVER NOT FOUND");
    	} finally {
        	try {
            	if (rs != null) rs.close();
            	if(stmt != null) stmt.close();
            	if(con != null) con.close();
        	} catch (SQLException e) {
            	System.err.println("Error while releasing resources");
        	}
    	}

	}
}
